
---
author:
- Yann Kervran
- Jean Bernard Marcon
- Christophe Masutti
rights: Creative Commons BY SA
description: Manuel de création d’epubs
title: Produire un epub Framabook
lang: fr-FR
subject: epub, Framabook, Framasoft
date: 2020-03-08
abstract: Ce manuel fournit les instructions de base pour déployer un pipeline de production d’epubs conformes aux recommandations d’accessibilité en ne se basant que sur des outils en ligne de commande dans un terminal shell Bash.
---


# Produire un epub Framabook

## Concepts de base

Le but est de permettre de générer des epubs 3.0 et/ou 3.2 valides pour l’accessibilité, qui soit conforme aux recommandations du Syndicat National de l’Édition^[Dans la [brochure](https://www.sne.fr/app/uploads/2020/12/ePubAccessibleCharteSNE_v1.0-version-nettoy%C3%A9e_couvOK.pdf) de la Commission Numérique - Groupe Normes et Standards de décembre 2020.] :

- un [EPUBCheck](https://github.com/w3c/epubcheck) sans erreur ni warning
- un niveau WCAG^[Web Content Accessibility Guidelines du World Wide Web Consortium (W3C).] au minimum AA (voire AAA dans la mesure du possible). Ce score s’obtient par le biais du contrôle d’[ACE](https://daisy.github.io/ace/)^[Accessibility Checker for EPUB du Consortium DAISY].

## Mise en place du pipeline

Le choix a été retenu de se baser sur une arborescence réfléchie pour fonctionner avec un ensemble de fichiers 'makefile', et n’intégrant que des commandes les plus simples et directes possibles dans un environnement de type BASH. Cela devrait permettre à la fois certaines automatisations tout en laissant la liberté de reprendre manuellement les points nécessaires à la spécificité d’un projet ou l’autre.

### Environnement de travail et logiciels nécessaires

Comme indiqué précédemment, il est nécessaire d’avoir un environnement de travail avec un terminal permettant d’entrer des commandes dans une console BASH. Il est possible que les options retenues soient compatibles avec d’autres interpréteurs de commandes Unix, mais cela devra être vérifié en amont avant de déployer la solution.

Liste des logiciels nécessaires :

- pandoc
- zip / unzip
- make
- git
- epubcheck
- ace (via nodejs, voir [la page d’installation du Consortium Daisy](https://daisy.github.io/ace/getting-started/installation/))

## Utilisation du pipeline

### Installation du dépôt

Un dépôt avec toutes les commandes a été créé sur [https://framagit.org/YannKervran/pipeline-framabook](https://framagit.org/YannKervran/pipeline-framabook).

Il fournit les outils de base pour déployer le pipeline. Il contient également un fichier source en markdown qui permet de générer le manuel d’usage de ce pipeline.

Pour l’installer, il suffit de se placer dans le répertoire de son choix puis de lancer la commande :

    git clone git@framagit.org:YannKervran/pipeline-framabook.git

### Organisation des répertoires

Les répertoires déployés au départ d’un projet sont les suivants :

- 'build' : il contiendra le résultat des commandes passées pour obtenir les fichiers finaux
- 'deflate' : il contiendra le résultat d’un epub tel que généré automatiquement par l’utilisation d’une commande pandoc sur les fichiers source
- 'src' : il contiendra les fichiers sources, textes en markdown compatible Pandoc^[Tels que décrits sur la page [
Pandoc Markdown
](https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html).]
- 'tmp' : il contiendra les fichiers temporaires et intermédiaires
- 'work' : il contiendra tous les éléments que nous modifierons manuellement en tant que surcouche à ce qui existe dans 'deflate' pour générer les fichiers finaux. L’arborescence de structure de l’epub doit être copiée dans 'work/replaced_files' pour que les fichiers qui y seront placés puissent remplacer ceux de mêmes noms générés automatiquement dans 'deflate', ou pour s’y ajouter si ceux-ci n’ont pas été générés automatiquement.

Comme certains dossiers du pipeline sont initialement vides à l’installation du pipeline, il faut veiller à ce que le répertoire créé par la commande précédente (« pipeline-framabook » par défaut) contienne donc bien les répertoires suivants (au besoin, les créer) :

- build
- deflate
- src
- tmp
- work/replaced_files

Il suffit ensuite de taper depuis la racine du dépôt la commande :

    make draft

pour obtenir dans le sous-répertoire 'build' le magit addnuel d’utilisation sous format epub. La source en est bien évidemment dans le sous-répertoire 'src'.


### Les commandes de base

Les commandes sont des commandes de type 'make' et font appels aux différents 'makefiles' disséminés dans les répertoires.

#### firstpass

Génère le contenu du répertoire 'deflate' à partir des fichiers markdown du répertoire 'src'. Ceux-ci doivent être numérotés dans l’ordre d’inclusion si celui-ci a une importance.

#### dezip

Extrait le contenu d’un fichier epub (veiller à ce qu’il soit unique) situé dns le répertoire 'src' vers le répertoire 'deflate' afin d’en permettre la modification/la reprise.

#### epub

Construit un fichier epub nommé 'monepub_final.epub' dans le répertoire 'build'. Il se base sur le contenu du répertoire 'tmp/epub' mis en place par sa dépendance, **tmpcopy**.

#### tmpcopy

Construit dans 'tmp/epub' l’arborescence des fichiers qui sera placée dans l’epub, avec le contenu de 'deflate', sur lequel celui de 'work/replaced_files' vient s’ajouter; Si ce dernier répertoire contient des fichiers de même nom que ceux de 'deflate', ils seront remplacés. 

#### draft

Combine les opérations 'firstpass' et 'epub' en une seule aux fins de visualiser rapidement le résultat du pipeline.

#### validate

Teste les fichiers epub du répertoire 'build' pour valider leur contenu :

- 'epubcheck' avec une sortie dans un fichier 'build/epucheck_report.json'
- 'ace' avec une sortie json et hml dans le répertoire 'build/ace_reports'

#### clean

Efface le contenu des répertoires 'deflate', 'build' et 'tmp' pour opérer sur une base propre.

### Préparer le pipeline pour un nouveau projet

#### Suivi de version avec git

Si vous avez l’intention de versionner votre projet avec git, il est très important de modifier en conséquence le fichier '.gitignore' situé à la racine. Il a été configuré pour ne pas suivre les fichiers de test et ne sera donc pas adapté aux avancées d’une mise en production collaborative. Il faudra en particulier supprimer la ligne 3 qui fait en sorte de ne pas suivre le répertoire 'work' et 11, qui exclue aussi 'src'. Vous pouvez également voir si vous souhaitez garder l’exclusion de suivi ligne 7 du répertoire 'deflate'.

#### Dépôt des documents dans /src

Il faut déposer dans le répertoire 'src' tout ce qui servira de point de départ au projet, selon les cas :

- les fichiers markdown, numérotés dans leur ordre d’inclusion. Ils doivent comporter comme en-tête une section YAML avec les indications de métadonnées nécessaires.
- un fichier 'cover.png' qui servira de couverture à l’epub généré depuis les fichiers markdown.
- un fichier epub, s’il s’agit d’en remanier le contenu. Dans ce cas, il n’est pas nécessaire d’inclure un fichier 'cover.png', la couverture sera celle d’origine pour le point de départ des modifications.

