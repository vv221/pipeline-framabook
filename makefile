# Inclusion conditionnelle de la couverture
COVER      := src/cover.png
COVER_FILE := $(wildcard $(COVER)) # "src/cover.png" s’il est présent, une variable vide sinon
ifneq ($(COVER_FILE),)
	DRAFT_PANDOC_OPTIONS := --epub-cover-image=$(COVER)
endif

# Liste des fichiers à remplacer
REPLACED_FILES := $(wildcard work/replaced_files/*)

firstpass:
	@ rm -Rf deflate/*
	@ pandoc src/*.md $(DRAFT_PANDOC_OPTIONS) -o tmp/draft1.epub
	@ unzip tmp/draft1.epub -d deflate
	@ rm tmp/draft1.epub

dezip:
	@ rm -Rf deflate/mimetype deflate/META-INF/ deflate/EPUB/ deflate/OEBPS/
	@ unzip src/*.epub -d deflate

epub: tmpcopy
	mkdir --parents build
	cd tmp/epub && zip -rX ../../build/monepub_final.epub *

tmpcopy:
	mkdir -p tmp/epub
	cp -r deflate/* tmp/epub
ifneq ($(REPLACED_FILES),)
	cp -r work/replaced_files/* tmp/epub
endif

draft: firstpass epub

validate: build/monepub_final.epub
	@ - epubcheck -j build/epucheck_report.json build/monepub_final.epub # Le - est nécessaire en début de commande make pour que make ne s’arrête pas si il reçoit une erreur de cette commande
	@ ace --force --outdir build/ace_reports build/monepub_final.epub

clean:
	@ rm -Rf deflate
	@ rm -Rf build
	@ rm -Rf tmp


